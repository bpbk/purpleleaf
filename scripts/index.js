$(function() {
  var form = $('#ajax-contact');
  var formMessages = $('#form-messages');
  var $slickContainer = $('.description_container');

  $(document).ready(function(){
    setTimeout(function(){
      var i = 0;
      while(i < $('.pp_triangle').length){
        const polygon = $('#fill_'+i);
        setTimeout(function(){
          polygon.addClass('active');
        }, 40 * i);
        i++;
      }

      setTimeout(function(){
        $('.purple_text').addClass('active');
        setTimeout(function(){
          $('.leaf_text').addClass('active');
          setTimeout(function() {
            $slickContainer.addClass('active').slick('slickSetOption', 'autoplay', true, true);
          },300)
        }, 600);
      }, $('.pp_triangle').length * 60);
    }, 200);
  });

  buildDescriptionSlick();
  function buildDescriptionSlick(){
    $slickContainer.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        fade: true,
        speed: 2000,
        autoplay:false,
        autoplaySpeed: 5000
    });
  }

  // FORM FUNCTIONS

  $('.demo').on('click', function(e){
    e.preventDefault();
    $('.footer').toggleClass('active');
    if($('.demo').html() == 'request a free demo'){
      $('.demo').html('close');
    }else{
      $('.demo').html('request a free demo');
      // Clear the form.
      $('#name').val('');
      $('#email').val('');
    }
  });

  $(form).submit(function(event) {
    // Stop the browser from submitting the form.
    event.preventDefault();
    var formData = $(form).serialize();
    $.ajax({
      type: 'POST',
      url: $(form).attr('action'),
      data: formData
    }).done(function(response) {
      // Make sure that the formMessages div has the 'success' class.
      $(formMessages).removeClass('error');
      $(formMessages).addClass('success');

      // Set the message text.
      $(formMessages).text(response);

      // Clear the form.
      $('#name').val('');
      $('#email').val('');
    }).fail(function(data) {
      // Make sure that the formMessages div has the 'error' class.
      $(formMessages).removeClass('success');
      $(formMessages).addClass('error');

      // Set the message text.
      if (data.responseText !== '') {
          $(formMessages).text(data.responseText);
      } else {
          $(formMessages).text('Oops! An error occured and your message could not be sent.');
      }
    });
  });

});
